package ciphers;

import java.util.ArrayList;
import java.util.List;

public class CipherUtils {

	public static boolean isLowerCase(char character) {
		return 'a' <= character && character <= 'z' || character == 'ñ';
	}

	public static boolean isUpperCase(char character) {
		return 'A' <= character && character <= 'Z' || character == 'Ñ';
	}

	public static boolean isAlphabetic(char character) {
		return isLowerCase(character) || isUpperCase(character);
	}

	public static char shift(char character, int positions) {
		int offset = alphabetOffset(character);
		offset = (offset + positions) % 26;
		if (offset < 0) offset += 26;
		return alphabetCharacter(offset, isLowerCase(character));
	}

	public static int alphabetOffset(char character) {
		if (character == 'ñ' || character == 'Ñ') return 26;
		char first = isLowerCase(character) ? 'a' : 'A';
		return character - first;
	}

	public static char alphabetCharacter(int offset, boolean lowerCase) {
		if (offset == 26) return lowerCase ? 'ñ' : 'Ñ';
		return (char) ((lowerCase ? 'a' : 'A') + offset);
	}

	public static List<Character> getAlphabet() {
		List<Character> alphabet = new ArrayList<Character>(27);
		for (char c = 'a'; c <= 'z'; c++) {
			alphabet.add(c);
		}
		alphabet.add('ñ');
		return alphabet;
	}

	public static int gcd(List<Integer> numbers) {
		int gcd = 0;
		for (Integer n : numbers) {
			gcd = gcd(gcd, n);
		}
		return gcd;
	}

	public static int gcd(int a, int b) {
		int c;
		while (a > 0) {
			c = a; a = b % a; b = c;
		}
		return b;
	}

}
