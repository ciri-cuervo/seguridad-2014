package ciphers;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HomophonicCipher {

	protected List<Character> reverseMapping;

	public HomophonicCipher(Map<Character, List<Character>> characterMapping) {
		reverseMapping = CipherUtils.getAlphabet();
		for (Entry<Character, List<Character>> entry : characterMapping.entrySet()) {
			for (Character character : entry.getValue()) {
				reverseMapping.set(CipherUtils.alphabetOffset(character), entry.getKey());
			}
		}
	}

	public String encode(@SuppressWarnings("unused") String plainText) {
		throw new UnsupportedOperationException("Not implemented, yet");
	}

	public String decode(String cipherText) {
		char[] characters = cipherText.toCharArray();
		for (int i = 0; i < characters.length; i++) {
			if (CipherUtils.isAlphabetic(characters[i])) {
				characters[i] = reverseMapping.get(CipherUtils.alphabetOffset(characters[i]));
			}
		}
		return new String(characters);
	}

}
