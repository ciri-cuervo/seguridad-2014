package ciphers;

public class CaesarCipher {

	protected int key;

	public CaesarCipher(int key) {
		this.key = key;
	}

	public String encode(String plainText) {
		char[] characters = plainText.toCharArray();
		for (int i = 0; i < characters.length; i++) {
			if (CipherUtils.isAlphabetic(characters[i])) {
				characters[i] = CipherUtils.shift(characters[i], key);
			}
		}
		return new String(characters);
	}

	public String decode(String cipherText) {
		char[] characters = cipherText.toCharArray();
		for (int i = 0; i < characters.length; i++) {
			if (CipherUtils.isAlphabetic(characters[i])) {
				characters[i] = CipherUtils.shift(characters[i], -key);
			}
		}
		return new String(characters);
	}

}
