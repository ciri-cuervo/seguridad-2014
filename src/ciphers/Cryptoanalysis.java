package ciphers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Cryptoanalysis {

	public static double[] getHistogram(String text) {
		double[] histogram = new double[27];
		// count characters
		for (int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			int offset = CipherUtils.alphabetOffset(character);
			histogram[offset] += 1.0D;
		}
		// calculate percentages
		for (int i = 0; i < histogram.length; i++) {
			histogram[i] /= text.length();
		}
		return histogram;
	}

	public static List<Character> getSortedHistogram(double[] histogram) {
		List<Character> alphabet = CipherUtils.getAlphabet();
		Collections.sort(alphabet, new HistogramComparator(histogram));
		return alphabet;
	}

	public static List<Character> createMapping(List<Character> keys, List<Character> values) {
		List<Character> mapping = CipherUtils.getAlphabet();
		for (int i = 0; i < keys.size() && i < 5; i++) {
			mapping.set(CipherUtils.alphabetOffset(keys.get(i)), values.get(i));
		}
		return mapping;
	}

	public static String getNPositionChars(String msg, int n, int keyLength) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < msg.length(); i++) {
			if (i % keyLength == n) {
				str.append(msg.charAt(i));
			}
		}
		return str.toString();
	}

	public static String replaceNPositionChars(String msg, int n, int keyLength, List<Character> replacement) {
		char[] characters = msg.toCharArray();
		for (int i = 0; i < characters.length; i++) {
			if (i % keyLength == n) {
				characters[i] = replacement.get(CipherUtils.alphabetOffset(characters[i]));
			}
		}
		return new String(characters);
	}

	public static int getVigenerePasswordLength(String cipherText, String token) {
		List<Integer> lengths = new ArrayList<Integer>();
		int fromIndex = 0;
		int pos;

		while ((pos = cipherText.indexOf(token, fromIndex)) != -1) {
			if (fromIndex > 0) { // discards first match
				int length = pos - fromIndex + token.length();
				lengths.add(length);
			}
			fromIndex = pos + token.length();
		}

		// returns greatest common divisor
		return CipherUtils.gcd(lengths);
	}

	private static class HistogramComparator implements Comparator<Character> {
		private double[] histogram;

		public HistogramComparator(double[] histogram) {
			this.histogram = histogram;
		}

		public int compare(Character c1, Character c2) {
			int offset1 = CipherUtils.alphabetOffset(c1.charValue());
			int offset2 = CipherUtils.alphabetOffset(c2.charValue());
			if (this.histogram[offset1] < this.histogram[offset2]) return 1;
			if (this.histogram[offset1] > this.histogram[offset2]) return -1;
			return 0;
		}
	}

}