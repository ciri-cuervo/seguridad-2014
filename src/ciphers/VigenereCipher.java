package ciphers;

public class VigenereCipher {

	protected char[] key;

	public VigenereCipher(String key) {
		this.key = key.toCharArray();
	}

	public String encode(String plainText) {
		char[] characters = plainText.toCharArray();
		int keyIndex = 0;
		for (int i = 0; i < characters.length; i++) {
			if (CipherUtils.isAlphabetic(characters[i])) {
				int shift = CipherUtils.alphabetOffset(key[keyIndex]);
				characters[i] = CipherUtils.shift(characters[i], shift);
				keyIndex = (keyIndex + 1) % key.length;
			}
		}
		return new String(characters);
	}

	public String decode(String cipherText) {
		char[] characters = cipherText.toCharArray();
		int keyIndex = 0;
		for (int i = 0; i < characters.length; i++) {
			if (CipherUtils.isAlphabetic(characters[i])) {
				int shift = CipherUtils.alphabetOffset(key[keyIndex]);
				characters[i] = CipherUtils.shift(characters[i], -shift);
				keyIndex = (keyIndex + 1) % key.length;
			}
		}
		return new String(characters);
	}

}
