package practico2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ciphers.HomophonicCipher;

public class Ejercicio3 {

	public static void main(String[] args) {

		String msg = "ab wz nrysx em klp foijdc tz uvaqgx wp nrh";
		Map<Character, List<Character>> mapping = new HashMap<Character, List<Character>>();
		mapping.put('c', Arrays.asList('f'));
		mapping.put('d', Arrays.asList('e'));
		mapping.put('e', Arrays.asList('a', 'w', 'r', 'm', 'i', 't'));
		mapping.put('g', Arrays.asList('j'));
		mapping.put('i', Arrays.asList('y', 'o'));
		mapping.put('l', Arrays.asList('z', 'k'));
		mapping.put('n', Arrays.asList('b', 's'));
		mapping.put('o', Arrays.asList('x', 'l', 'd'));
		mapping.put('r', Arrays.asList('n', 'q'));
		mapping.put('s', Arrays.asList('p', 'c'));
		mapping.put('t', Arrays.asList('u', 'g'));
		mapping.put('u', Arrays.asList('v'));
		mapping.put('y', Arrays.asList('h'));

		HomophonicCipher cipher = new HomophonicCipher(mapping);
		System.out.println(cipher.decode(msg));
	}

}
