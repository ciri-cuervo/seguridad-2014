package practico2;

import ciphers.CaesarCipher;

public class Ejercicio1 {

	public static void main(String[] args) {

		String msg = "Yznxdamvm zg zezmxdxdj 3, lpz nz zixpziomv xdamvyj xji zg vgbjmdohj yz Qdbzizmz xji xgvqz ’MUHIJ’.";
		int key = 21;

		CaesarCipher cipher = new CaesarCipher(key);
		System.out.println(cipher.decode(msg));
	}

}
