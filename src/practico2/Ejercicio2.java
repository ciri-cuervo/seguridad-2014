package practico2;

import ciphers.VigenereCipher;

public class Ejercicio2 {

	public static void main(String[] args) {

		String msg = "Udepwwqme zr rutizdzgs wqmfs lsuywqzzqc cz obrzeupothaa vflascehon eld er aldegfr dz yo kznyo 2: ’ra im bixek sd jxc tfhvqq ky giohfj jd eqt’";
		String key = "RZMNO";

		VigenereCipher cipher = new VigenereCipher(key);
		System.out.println(cipher.decode(msg));
	}

}
